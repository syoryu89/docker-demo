FROM ruby:2.6.3

# RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN apt-get update -qq && \
    apt-get install -y vim && \
    apt-get install -y build-essential && \
    apt-get install -y libpq-dev && \
    apt-get install -y nodejs

# ワーキングディレクトリの設定
RUN mkdir /app
WORKDIR /app

# gemfileを追加する
ADD src/Gemfile /app/Gemfile
ADD src/Gemfile.lock /app/Gemfile.lock

# gemfileのinstall
RUN bundle install
COPY src /app