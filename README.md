# Ruby on Rails using Docker

## Docker Ready

- docker-compose.ymlの内容でコンテナが作成

```bash
$ docker-compose run web rails new . --force --database=mysql
```

- Gemfile修正

```bash
$ docker-compose build
```

## DB migrate

- `database.yml` を確認
- password：docker-compose.ymlに設定したmysqlのルートユーザーのパスワード
- host：docker-compose.ymlに設定したMySQLコンテナ名

```bash
$ docker-compose run web rake db:create
```

- `Mysql2::Error::ConnectionError: Access denied for user` が出たら以下参照
- https://github.com/docker-library/mysql/issues/51

## Rails start

```bash
$ docker-compose up
```